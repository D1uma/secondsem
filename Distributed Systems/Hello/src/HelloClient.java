import java.rmi.*;
import java.rmi.registry.*;
public class HelloClient {
public static void main(String [] args) {
   try {
	/*if (args.length < 1) {
		System.out.println("Usage: java HelloClient <server host>"); 
		return;}*/
	String host="localhost";//args[0];
	Registry registry=LocateRegistry.getRegistry(host);
	Hello h=(Hello) registry.lookup("Hello");
	
	String res=h.sayHello();
	System.out.println(res);
	} catch (Exception e){
		System.err.println("Error on the client side "+e);
	}
	}
	}