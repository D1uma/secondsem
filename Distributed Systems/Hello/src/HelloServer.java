import java.rmi.*;
import java.rmi.server.*;
import java.rmi.registry.*;

public class HelloServer{
	public static void main(String args[]){
		try{
			HelloImpl h=new HelloImpl ("Hello World");
			Hello h_stub=(Hello) UnicastRemoteObject.exportObject(h,0); //create the reference
			
			Registry registry=LocateRegistry.getRegistry();//send this reference to the registry
			registry.bind("Hello",h_stub);
			
			System.out.println("Server is ready");
		}catch (Exception e){
		System.err.println("Error on the server "+e);
		e.printStackTrace();
	}
}
}